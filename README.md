# Knowledge Base

This project contains instructions, snippets, common concepts, and explanations for software development, development workspace setup, operating systems, and that kind of content in MarkDown Files.

# Contributions

If you want to contribute with your experience to add new content or to improve the existing one, which is very appreciated, you have to:

- Open a MR (Merge Request)
- Select at least one reviewer

After that, I'll check the MR and request changes if needed in order to improve the work.

Finally, if everything looks good, your work will be merged to the master branch.

# Branch and MR naming
I think naming the MR like the branch is ok.

### New content:
Use the "add" prefix. You can add folders and files.

### Fix:
Use the "fix" prefix. You can add, remove, or update files and folders.

### Improvement:
Use the "improve" prefix. You can add, remove, or update files and folders.

```
[action][topic][file]
Branch:         add-linux-mint-netbeans-installation
Merge Request:  Add: linux mint netbeans installation
```

Example file and folder structure:
```
-linux
  -netbeans-installation-linux-mint.md
```
