# Comandos Git

#### Clonar un repositorio
```
git clone <url_repo>
```
#### Descargar repositorio
```
$>_ git pull
```


### Crear branch
##### Ver la branch de trabajo
```
$>_ git branchs
```
##### Ver todas las branches
```
$>_ git branch --all
```

##### Moverse a una branch
```
$>_ git checkupt <nombre_branch>
```


##### Crear una branch y moverse a ella
```
$>_ git checkuot -b <nombre_branch>
```

#
### Cargar cambios 
##### Ver status. Nos permite visualizar cambios "Antes de guardar cambios"
```
$>_ git status
```
##### Agregamos los cambios al Stage
```
# agregar un archivo 
$>_ git add "./ruta/archivo.exten"

# Aregar una carpeta <Todo lo que contenga la carpeta>
$>_ git add "./ruta/*.*"

# agregar todo *
$>_ git add .

```
##### Confirmar lo que se enviará 
```
# Commit con mensaje corto
$>_ git commit -m "Tu mensaje"

# Commit con mensaje largo "Abirá un editir en la consola"
$>_ git commit
```

##### Subir cambios 
```
$>_ git push
```
 

