## The following commands and instructions are for Debian based distribution

# Java Runtime Enviroment Installation ([JRE](https://www.java.com/en/download/))

1. First, verify if you have the `JRE` already installed:

```
  $ java -version
```

2. If you get an error, you have to install it with the following command:

```
  $ sudo apt install default-jre
```

# Install [OpenJDK](https://openjdk.java.net/)
In a terminal, use the following command to install [OpenJDK (Java Development Kit)](https://openjdk.java.net/):

Default version:
```
$ sudo apt install default-jdk
```
Specific version:
```
$ sudo apt install openjdk-8-jdk
```

# Set JAVA_HOME Path &  Java bin directory
In Linux Mint 19.2, the enviroment paths are in the profile file:

```
$ /home/yourusername/.bashrc
```
Once you found it, open the file with a text editor and add the following content at the end:
```
...

# JAVA PATH
export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
export PATH=$PATH:$JAVA_HOME/bin

```
Make sure that your environment variables point to a valid installation of `JDK` on your machine, specially if you have installed the `JDK` manually.

You can search for the `Java JDK` directory with your file explorer, copy the directory absolute path and paste it in the `JAVA_HOME` variable value.


To check whether your `JAVA_HOME` path has been successfully saved, enter the following command to check.

```
$ echo $PATH
```
You will see something like this. Look for the Java `JDK directory`.
```
/usr/local/apache-maven-3.6.3/bin:/home/linuxbrew/.linuxbrew/bin:/home/linuxbrew/.linuxbrew/sbin:/usr/local/apache-maven-3.6.3/bin:/home/linuxbrew/.linuxbrew/bin:/home/linuxbrew/.linuxbrew/sbin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/home/yourusername/.dotnet/tools:/home/yourusername/.dotnet/tools:/home/yourusername/Android/Sdk/emulator:/home/yourusername/Android/Sdk/tools:/home/yourusername/Android/Sdk/tools/bin:/home/yourusername/Android/Sdk/platform-tools:/home/yourusername/Android/Sdk/emulator:/home/yourusername/Android/Sdk/tools:/home/yourusername/Android/Sdk/tools/bin:/home/yourusername/Android/Sdk/platform-tools:/usr/lib/jvm/java-8-openjdk-amd64/bin
```

# Test Java setup
You have successfully installed `OpenJDK` on your machine. You can verify your installation by entering the following command on your terminal.
```
$ java -version
```
You will see something like this:
```
openjdk version "11.0.7" 2020-04-14
OpenJDK Runtime Environment (build 11.0.7+10-post-Ubuntu-2ubuntu218.04)
OpenJDK 64-Bit Server VM (build 11.0.7+10-post-Ubuntu-2ubuntu218.04, mixed mode, sharing)
```


| Sources |
|---------|
|https://itsfoss.com/install-java-ubuntu/|
|https://vitux.com/how-to-setup-java_home-path-in-ubuntu/|